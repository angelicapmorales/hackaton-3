from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, UsernameField
from django import forms
from .models import ClientModel, LibrarianModel


class ClientUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = ClientModel
        fields = UserCreationForm.Meta.fields + ('custom_field',)


class LibrarianUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = LibrarianModel
        fields = UserCreationForm.Meta.fields + ('custom_field',)


class ClientAuthForm(AuthenticationForm):

    username = UsernameField()
    password = forms.CharField()

    class Meta:
        fields = ['username', 'password']
