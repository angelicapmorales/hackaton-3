from django.contrib.auth.models import User
from django.db import models


# Create your models here.


class Editorial(models.Model):
    name = models.CharField(max_length=150, null=False, blank=False)

    def __str__(self):
        return self.name


class State(models.Model):
    state = models.BooleanField(null=False)


class Book(models.Model):
    title = models.CharField(max_length=200, null=False)
    author = models.CharField(max_length=150, null=False)
    editorial = models.ForeignKey(Editorial, on_delete=models.CASCADE)
    stock = models.IntegerField(null=False, blank=False)
    publication_date = models.DateField()
    state = models.ForeignKey(State, on_delete=models.CASCADE)

    def __str__(self):
        return self.title


class ClientModel(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    address = models.CharField(max_length=150)

    def __str__(self):
        return self.user.first_name + self.user.last_name


class LibrarianModel(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    address = models.CharField(max_length=150)

    def __str__(self):
        return self.user.first_name + self.user.last_name

class Booking(models.Model):
    date = models.DateField(null=False, blank=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
