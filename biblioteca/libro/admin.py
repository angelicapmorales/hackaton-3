from django.contrib import admin

# Register your models here.
from libro.models import Editorial, Book, ClientModel, LibrarianModel, Booking

admin.site.register(Editorial)
admin.site.register(Book)
admin.site.register(ClientModel)
admin.site.register(LibrarianModel)
admin.site.register(Booking)
