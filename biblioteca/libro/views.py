from django.contrib import messages
from django.contrib.auth import login, authenticate, logout
from django.shortcuts import render, redirect
from .forms import LibrarianUserCreationForm, ClientUserCreationForm, ClientAuthForm
from django.views.generic import TemplateView
# Create your views here.


def client_create(request):
    """
    Manage to create a new user
    """
    if request.method == "POST":
        form = ClientUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            messages.success(request, 'Usuario creado')
            return redirect('/')
        messages.error(request, "No se pudo crear el usuario")
    form = ClientUserCreationForm
    context = {'login_form', form}
    return render(request, 'base.html', context)


def client_login(request):
    if request.method == "POST":
        form = ClientAuthForm(request.POST)
        if form.is_valid:
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('/')
            messages.error(request, 'Usuario no valido')
        messages.error(request, 'usuario no valido')
    context = {'form', ClientAuthForm}
    messages.success(request, 'bienvenido')
    return render(request=request, context=context, template_name='base.html')


def logout_view(request):
    logout(request)
    return redirect('home/')


class Home(TemplateView):
    template_name = 'home.html'
